package com.cuiweiyou.cutor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

/**
 * 初始化UI的工具
 */
public class CutorUIManager {
    private static CutorFrame cutorFrame;

    private CutorUIManager() {
    }

    public static Container initUI(CutorFrame frame, int frameWidth, int frameHeight) {
        cutorFrame = frame;
        int centerPanelHeight = frameHeight / 13 * 11;
        int northPanelHeight = frameHeight / 13;
        int southPanelHeight = frameHeight / 13;

        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.add(BorderLayout.SOUTH, getSouthPanel(frameWidth, southPanelHeight));
        panel.add(BorderLayout.NORTH, getFilePathPanel(frameWidth, northPanelHeight));
        panel.add(BorderLayout.CENTER, getVideoPlayPanel(frameWidth, centerPanelHeight));
        
        return panel;
    }

    /*
     * 时间段选择和添加和剪辑按钮
     *
     * @param panelWidth
     * @param panelHeight
     * @return */
    private static Component getSouthPanel(int panelWidth, int panelHeight) {
        int timeCutPanelWidth = panelWidth / 4 * 3;
        int timeCutPanelHeight = (int) (panelHeight / 5 * 4);

        int rightPWidth = panelWidth / 4 * 1;
        int rightPHeight = (int) (panelHeight / 5 * 4);

        cutorFrame.cutButton = new JButton("剪辑");
        cutorFrame.cutButton.setPreferredSize(new Dimension(rightPWidth - 10, rightPHeight));

        JPanel rightPanel = new JPanel();
        rightPanel.setBackground(Color.WHITE);
        rightPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        rightPanel.setPreferredSize(new Dimension(rightPWidth, rightPHeight));
        rightPanel.add(cutorFrame.cutButton);

        JPanel panel = new JPanel();
        panel.setBackground(Color.WHITE);
        panel.setLayout(new BorderLayout());
        panel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        panel.setPreferredSize(new Dimension(panelWidth, panelHeight));
        panel.add(BorderLayout.WEST, getTimeCutPanel(timeCutPanelWidth, timeCutPanelHeight));
        panel.add(BorderLayout.EAST, rightPanel);
        return panel;
    }

    /**
     * 顶部视频文件路径frame
     *
     * @param panelWidth
     * @param panelHeight
     * @return
     */
    private static Component getFilePathPanel(int panelWidth, int panelHeight) {
        int fieldWidth = (int) (panelWidth / 10 * 9);
        int btnWidth = (int) (panelWidth / 10 * 0.8);

        cutorFrame.selectedFilePathField = new JTextField("请选择视频文件");
        cutorFrame.selectedFilePathField.setEditable(false);
        cutorFrame.selectedFilePathField.setPreferredSize(new Dimension(fieldWidth, (int) (panelHeight / 3 * 2)));

        cutorFrame.selectFileButton = new JButton("...");
        cutorFrame.selectFileButton.setPreferredSize(new Dimension(btnWidth, (int) (panelHeight / 3 * 2)));

        JPanel panel = new JPanel();
        panel.setBackground(Color.WHITE);
        panel.setLayout(new FlowLayout(FlowLayout.LEFT));
        panel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        panel.add(cutorFrame.selectedFilePathField);
        panel.add(cutorFrame.selectFileButton);

        return panel;
    }

    /**
     * 中部播放器和截取列表frame
     *
     * @param panelWidth
     * @param panelHeight
     * @return
     */
    private static Component getVideoPlayPanel(int panelWidth, int panelHeight) {
        int leftPWidth = (int) (panelWidth / 4 * 3);
        int rightPWidth = (int) (panelWidth / 4);

        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.setBorder(BorderFactory.createEmptyBorder());
        panel.setPreferredSize(new Dimension(panelWidth, panelHeight));
        panel.add(BorderLayout.WEST, getVidePanel(leftPWidth, panelHeight));
        panel.add(BorderLayout.EAST, getClipsPanel(rightPWidth, panelHeight));

        return panel;
    }

    /**
     * 视频播放器容器
     *
     * @param panelWidth
     * @param panelHeight
     * @return
     */
    private static Component getVidePanel(int panelWidth, int panelHeight) {
        int playerHeight = (int) (panelHeight / 12 * 11);
        int barHeight = (int) (panelHeight / 12);

        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        panel.setPreferredSize(new Dimension(panelWidth, panelHeight));
        panel.add(BorderLayout.NORTH, getPlayerPanel(panelWidth, playerHeight));
        panel.add(BorderLayout.CENTER, getPlayBarPanel(panelWidth, barHeight));

        return panel;
    }

    // 播放器
    private static Component getPlayerPanel(int panelWidth, int panelHeight) {
        cutorFrame.playView = new PlayView();
        cutorFrame.playView.setLayout(new BoxLayout(cutorFrame.playView, BoxLayout.Y_AXIS));
        cutorFrame.playView.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        cutorFrame.playView.setPreferredSize(new Dimension(panelWidth, panelHeight));
        return cutorFrame.playView;
    }

    // 播放器控制台
    private static Component getPlayBarPanel(int panelWidth, int panelHeight) {
        JLabel separator1 = new JLabel("  ");
        cutorFrame.playDurationLabel = new JLabel("00:00:00");
        JLabel separatorLabel = new JLabel(" / ");
        cutorFrame.playTimeLabel = new JLabel("00:00:00");
        JLabel separator2 = new JLabel("  ");
        cutorFrame.playTimeSlider = new JSlider();
        cutorFrame.playTimeSlider.setMinimum(0);
        cutorFrame.playTimeSlider.setMaximum(0);
        cutorFrame.playTimeSlider.setValue(0);
        cutorFrame.playPauseButton = new JButton(" || ");
        cutorFrame.playStopButton = new JButton(" ▇ ");
        cutorFrame.playPauseButton.setMaximumSize(new Dimension(cutorFrame.playPauseButton.getMinimumSize().width, (int) (panelHeight / 4 * 3.5)));
        cutorFrame.playStopButton.setMaximumSize(new Dimension(cutorFrame.playPauseButton.getMinimumSize().width, (int) (panelHeight / 4 * 3.5)));

        JPanel panel = new JPanel();
        panel.setBackground(Color.WHITE);
        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
        panel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        panel.setPreferredSize(new Dimension(panelWidth, panelHeight));

        panel.add(separator1);
        panel.add(cutorFrame.playDurationLabel);
        panel.add(separatorLabel);
        panel.add(cutorFrame.playTimeLabel);
        panel.add(separator2);
        panel.add(cutorFrame.playPauseButton);
        panel.add(cutorFrame.playTimeSlider);
        panel.add(cutorFrame.playStopButton);

        return panel;
    }

    /**
     * 截取列表和全选按钮
     *
     * @param panelWidth
     * @param panelHeight
     * @return
     */
    private static Component getClipsPanel(int panelWidth, int panelHeight) {
        int selectHeight = panelHeight / 12;
        int listHeight = panelHeight / 12 * 11;

        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.setPreferredSize(new Dimension(panelWidth, panelHeight));
        panel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        panel.add(BorderLayout.NORTH, getClipsListPanel(panelWidth, listHeight));
        panel.add(BorderLayout.CENTER, getClipsSelectPanel(panelWidth, selectHeight));

        return panel;
    }

    /**
     * 截取列表
     *
     * @param panelWidth
     * @param panelHeight
     * @return
     */
    private static Component getClipsListPanel(int panelWidth, int panelHeight) {
        cutorFrame.clipsListView = new JList<>();
        cutorFrame.clipsListView.setBackground(Color.WHITE);

        cutorFrame.clipsListView.setFixedCellHeight(cutorFrame.clipsListView.getFixedCellHeight() + 30);
        cutorFrame.clipsListView.setCellRenderer(new IconHeadCellRenderer());

        cutorFrame.clipsScrollPanel = new JScrollPane(cutorFrame.clipsListView);
        cutorFrame.clipsScrollPanel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        cutorFrame.clipsScrollPanel.setPreferredSize(new Dimension(panelWidth, panelHeight));

        return cutorFrame.clipsScrollPanel;
    }

    /**
     * 全选按钮
     *
     * @param panelWidth
     * @param panelHeight
     * @return
     */
    private static Component getClipsSelectPanel(int panelWidth, int panelHeight) {
        cutorFrame.selectAllClipsButton = new JButton("全选");
        cutorFrame.selectedLabel = new JLabel("片段数量：0，选择数量：0");

        JPanel panel = new JPanel();
        panel.setBackground(Color.WHITE);
        panel.setLayout(new BorderLayout());
        panel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        panel.setPreferredSize(new Dimension(panelWidth, panelHeight));
        panel.add(BorderLayout.WEST, cutorFrame.selectedLabel);
        panel.add(BorderLayout.EAST, cutorFrame.selectAllClipsButton);

        return panel;
    }

    /**
     * 时间段截取
     *
     * @param panelWidth
     * @param panelHeight
     * @return
     */
    private static Component getTimeCutPanel(int panelWidth, int panelHeight) {
        int leftRightWidth = panelWidth / 5;
        int leftLeftWidth = panelWidth / 5 * 4;

        JSeparator separator = new JSeparator(SwingConstants.CENTER);
        separator.setPreferredSize(new Dimension(100, 5));
        separator.setBackground(Color.BLACK);
        separator.setForeground(Color.BLACK);

        cutorFrame.startTimeButton = new JButton("起始");
        cutorFrame.startTimeButton.setPreferredSize(new Dimension(50, panelHeight));
        cutorFrame.stopTimeButton = new JButton("结束");
        cutorFrame.stopTimeButton.setPreferredSize(new Dimension(50, panelHeight));

        int tfWidth = (leftLeftWidth - 240) / 2;
        cutorFrame.startTimeTextField = new JTextField("00:00");
        cutorFrame.startTimeTextField.setPreferredSize(new Dimension(tfWidth, panelHeight));
        cutorFrame.startTimeTextField.setEditable(false);
        cutorFrame.stopTimeTextField = new JTextField("00:00");
        cutorFrame.stopTimeTextField.setPreferredSize(new Dimension(tfWidth, panelHeight));
        cutorFrame.stopTimeTextField.setEditable(false);

        JPanel cutPanel = new JPanel();
        cutPanel.setBackground(Color.WHITE);
        cutPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        cutPanel.setPreferredSize(new Dimension(leftLeftWidth, panelHeight));
        cutPanel.add(cutorFrame.startTimeButton);
        cutPanel.add(cutorFrame.startTimeTextField);
        cutPanel.add(separator);
        cutPanel.add(cutorFrame.stopTimeButton);
        cutPanel.add(cutorFrame.stopTimeTextField);

        cutorFrame.addClipsButton = new JButton("+");
        cutorFrame.addClipsButton.setPreferredSize(new Dimension(leftRightWidth - 20, panelHeight));

        JPanel addPanel = new JPanel();
        addPanel.setBackground(Color.WHITE);
        addPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        addPanel.setPreferredSize(new Dimension(leftRightWidth - 10, panelHeight));
        addPanel.add(cutorFrame.addClipsButton);

        JPanel leftPanel = new JPanel();
        leftPanel.setBackground(Color.WHITE);
        leftPanel.setLayout(new BorderLayout());
        leftPanel.setPreferredSize(new Dimension(panelWidth, panelHeight));
        leftPanel.add(BorderLayout.WEST, cutPanel);
        leftPanel.add(BorderLayout.EAST, addPanel);

        return leftPanel;
    }
}

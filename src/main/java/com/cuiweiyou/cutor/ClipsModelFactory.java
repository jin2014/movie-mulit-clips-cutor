package com.cuiweiyou.cutor;

import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;

/**
 * 为了应对java swing JList的数据体工厂
 * JList每次setModel时，model必须发生内存地址变化才会刷新
 * www.gaohaiyan.com
 */
public class ClipsModelFactory {

    private ClipsModelFactory() {
    }

    private static ClipsListModel clipsListModel;

    /**
     * 提取全部片段
     *
     * @return
     */
    public static ClipsListModel getAllClips() {
        if (null != clipsListModel) {
            boolean selectedAll = clipsListModel.isSelectedAll();
            List<ClipsModel> list = clipsListModel.getClipsList();
            List<ClipsModel> clipsList = new ArrayList<>(list);
            list.clear();
            clipsListModel = new ClipsListModel(clipsList);
            clipsListModel.setSelectedAll(selectedAll);
        } else {
            clipsListModel = new ClipsListModel();
        }
        return clipsListModel;
    }

    /**
     * 获取某个片段
     *
     * @return
     */
    public static ClipsModel getOneClips(int i) {
        if (null != clipsListModel) {
            return clipsListModel.getClipsList().get(i);
        }

        return null;
    }

    /**
     * 获取片段个数
     *
     * @return
     */
    public static int getClipsCount() {
        if (null != clipsListModel) {
            return clipsListModel.getClipsList().size();
        }

        return 0;
    }

    /**
     * 添加片段
     *
     * @param clips
     * @return
     */
    public static boolean addClips(ClipsModel clips) {
        if (null == clipsListModel) {
            clipsListModel = new ClipsListModel();
        }
        return clipsListModel.addClips(clips);
    }

    /**
     * 移除片段
     *
     * @param clips
     * @return
     */
    public static boolean removeClips(ClipsModel clips) {
        if (null != clipsListModel) {
            return clipsListModel.removeClips(clips);
        }
        return false;
    }

    /**
     * 选择或取消选择某个片段
     *
     * @param i 索引
     */
    public static void switchSelectedOneClips(int i) {
        ClipsModel model = clipsListModel.getClipsList().get(i);
        switchSelectedOneClips(model);
    }

    /**
     * 选择或取消选择某个片段
     *
     * @param clips 选择状态取反
     */
    public static void switchSelectedOneClips(ClipsModel clips) {
        clipsListModel.switchSelectedOneClips(clips);
    }

    /**
     * 全选或取消全选
     */
    public static void switchSelectedAllClips() {
        if (null != clipsListModel) {
            clipsListModel.switchSelectedAllClips();
        }
    }

    /**
     * 是否全选了
     *
     * @return
     */
    public static boolean isSelectedAll() {
        if (null == clipsListModel) {
            return false;
        }
        return clipsListModel.isSelectedAll();
    }

    /**
     * 获取选择的片段
     *
     * @return
     */
    public static List<ClipsModel> getSelectedClips() {
        if (null != clipsListModel) {
            return clipsListModel.getSelectedClips();
        }
        return new ArrayList<ClipsModel>();
    }

    /**
     * JList可用的AbstractListModel
     */
    private static class ClipsListModel extends AbstractListModel<ClipsModel> {
        private List<ClipsModel> clipsList;
        private boolean isSelectedAll = true;

        public ClipsListModel() {
            this.clipsList = new ArrayList<>();
        }

        public ClipsListModel(List<ClipsModel> clipsList) {
            this.clipsList = clipsList;
        }

        public List<ClipsModel> getClipsList() {
            return clipsList;
        }

        public boolean addClips(ClipsModel clips) { // todo 临时
            clipsList.add(clips);
            if (!clipsList.contains(clips)) {
                return true;
            }

            return true;
        }

        public boolean removeClips(ClipsModel clips) {
            return clipsList.remove(clips);
        }

        public void switchSelectedOneClips(ClipsModel clips) {
            if (clipsList.contains(clips)) {
                clips.setSelected(!clips.isSelected());
            }
        }

        public void switchSelectedAllClips() {
            isSelectedAll = !isSelectedAll;
            if (clipsList.size() > 0) {
                for (ClipsModel model : clipsList) {
                    model.setSelected(isSelectedAll);
                }
            }
        }

        public List<ClipsModel> getSelectedClips() {
            List<ClipsModel> tmp = new ArrayList<>();
            if (clipsList.size() > 0) {
                for (ClipsModel model : clipsList) {
                    if (model.isSelected()) {
                        tmp.add(model);
                    } else {
                        isSelectedAll = false;
                    }
                }
                return tmp;
            }

            return tmp;
        }

        public boolean isSelectedAll() {
            return isSelectedAll;
        }

        public void setSelectedAll(boolean selectedAll) {
            isSelectedAll = selectedAll;
        }

        @Override
        public int getSize() {
            return null == clipsList ? 0 : clipsList.size();
        }

        @Override
        public ClipsModel getElementAt(int i) {
            return clipsList.get(i);
        }
    }

}
